<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Funcion que le pasas un array con valores y ella te devuelve las veces que se repite cada valor
         * @param array $array es el conjunto de valores a utilizar
         * @param bool $devolverTodos si le indicas true te devuelve los valores que no se repiten tambien
         * @return int[] es el array con las frecuencias de cada uno de los valores del array de entrada
         */
        function elementosRepetidos($array, $devolverTodos = false) {
            $repeated = [];
            //$array=[1,1,2,3,2,1];

            foreach ((array) $array as $value) {
                $inArray = false;

                foreach ($repeated as $i => $rItem) {
                    if ($rItem['valor'] === $value) {
                        $inArray = true;
                        ++$repeated[$i]['cuenta'];
                    }
                }

                if (false === $inArray) {
                    //$i = count($repeated);
                    //$repeated[$i] = [];
                    //$repeated[$i]['valor'] = $value;
                    //$repeated[$i]['cuenta'] = 1;
                    $repeated[]=[
                        'valor' => $value,
                        'cuenta' => 1
                    ];
                    
                }
            }

            if ($devolverTodos==false) {
                foreach ($repeated as $i => $rItem) {
                    if ($rItem['cuenta'] === 1) {
                        unset($repeated[$i]);
                    }
                }
            }

            sort($repeated);

            return $repeated;
        }
        
        //$entrada=array(1,2,3,"a","a","b",1,1,1,1,1);
        $entrada=[1,1,2,2,1,4];
        var_dump(elementosRepetidos($entrada,false));
        
        ?>
    </body>
</html>
